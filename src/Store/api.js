import axios from "axios";

export async function getMovies() {
  let movies = [];
  const { data, error } = await axios(
    `https://api.themoviedb.org/3/movie/popular?api_key=f2f67cadf46e3bede42d6aae34920b38&language=en-US&page=1`
  );
  return data ? { data: data.results, totalpages: data.total_pages } : error;
}

export async function getMoviesByPage(page) {
  const { data, error } = await axios(
    `https://api.themoviedb.org/3/movie/popular?api_key=f2f67cadf46e3bede42d6aae34920b38&language=en-US&page=${page}`
  );
  return data ? data.results : error;
}

export async function searchMovie(text) {
  const { data, error } = await axios(
    `https://api.themoviedb.org/3/search/movie?api_key=f2f67cadf46e3bede42d6aae34920b38&language=en-US&query=${text}&page=1&include_adult=false`
  );
  return data ? data.results : error;
}

export async function rateMovie(number) {
  console.log("asd", number);
  const { data, error } = await axios({
    method: "post",
    url:
      "https://api.themoviedb.org/3/movie/546554/rating?api_key=f2f67cadf46e3bede42d6aae34920b38&guest_session_id=361a0a1795c65ad11853cef95d3f2fd7",
    data: {
      value: Number(number),
    },
  });
  return data ? data : error;
}
