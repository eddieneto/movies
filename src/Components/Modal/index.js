import React from "react";
import { Modal, Input } from "antd";
import "antd/dist/antd.css";
import { ContainerImage, Title, Rate } from "./styles";

const MovieModal = ({ visible, onOk, onCancel, movie, onChange }) => {
  const handleInput = (value) => {
    console.log("asd", value.target.value);
    onChange && onChange(value.target.value);
  };

  return (
    <Modal
      title={movie.title}
      visible={visible}
      onOk={onOk}
      onCancel={onCancel}
    >
      <ContainerImage
        img={`https://image.tmdb.org/t/p/w220_and_h330_face${movie.backdrop_path}`}
      />
      <Title><b>{movie.title}</b></Title>
      <Rate><b>Rate : {movie.vote_average}</b></Rate>
      <p>Para votar digite um numero entre 1 e 10</p>
      <Input placeholder="Rate movie" onChange={handleInput} />
    </Modal>
  );
};

export default MovieModal;
