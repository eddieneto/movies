import styled from "styled-components";

import {
  Card as AntdCard,
  AutoComplete as AntdAutoComplete,
  Pagination as AntdPagination,
} from "antd";

export const ContainerImage = styled.div`
  background-image: ${({ img }) => `url(${img})`};
  background-size: cover;
  background-position: center;
  height: 400px;
  border-radius: 15px;
  width: auto;
`;

export const Container = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  background-color: midnightblue;
`;

export const ContainerHearder = styled.div`
  padding-top: 10px;
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export const Title = styled.h2``;

export const Rate = styled.h4``;

