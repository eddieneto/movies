import styled from "styled-components";
import { Result as AntdResult } from 'antd';

export const Result = styled(AntdResult)`
  color: white;
  height: 745px;
`;