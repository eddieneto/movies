import React from "react";
import "antd/dist/antd.css";

import {Result} from "./styles"

const ErrorContent = () => {

  return (
    <Result
    status="404"
    title="404"
    subTitle="Algo de errado aconteceu... recarregue a pagina "
  />
  )
};

export default ErrorContent;
