import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import { Input, Spin, Alert, message } from "antd";
import { useMovies } from "./hook";
import {
  Container,
  ContainerHearder,
  ContainerMovies,
  ContainerImage,
  Card,
  ContentMovie,
  Title,
  SubTitle,
  Row,
  ImageTitle,
  HeaderText,
  AutoComplete,
  Pagination,
} from "./styles";

import MovieModal from "../../Components/Modal";
import ErrorContent from "../../Components/ErrorContent";
import movieIcon from "../../assets/images/cinema.png";

const Dashboard = () => {
  const [search, setSearch] = useState("");
  const [movie, setMovie] = useState({});
  const [load, setLoad] = useState(true);
  const [error, setError] = useState(false);
  const [visible, setVisible] = useState(false);
  const [page, setPage] = useState(1);
  const [rating, setRating] = useState(null);
  const [options, setOptions] = useState([]);

  const {
    movies,
    totalPages,
    setMovies,
    UseSearchMovie,
    UseMoviesByPage,
    UserateMovie,
    rateresponse,
  } = useMovies();

  const { Meta } = Card;

  useEffect(() => {
    setLoad(false);
  }, [movies]);

  useEffect(() => {
    setMovies();
  }, []);

  useEffect(() => {
    UseSearchMovie(search);
  }, [search]);

  useEffect(() => {
    if (rateresponse.success) {
      message.success(rateresponse.status_message);
      UseMoviesByPage(page);
    } else {
    }
  }, [rateresponse]);

  const handleSearch = (value) => {
    setOptions(value ? searchResult(value) : []);
  };

  const handleSelectMovie = (movie) => {
    setMovie(movie);
    showModal();
  };

  const searchResult = (query) => {
    setSearch(query);
  };

  const handleChangeSearch = (value) => {
    if (value === "") {
      UseMoviesByPage(page);
    }
  };

  const handlePageChange = (page) => {
    setPage(page);
    UseMoviesByPage(page);
  };

  const renderCardsMovies = () => {
    const rederCards = movies?.map((movie) => {
      const img =
        "https://image.tmdb.org/t/p/w220_and_h330_face" + movie.backdrop_path;
      return (
        <ContentMovie>
          <Card
            hoverable
            cover={<ContainerImage img={img} />}
            onClick={() => {
              handleSelectMovie(movie);
            }}
          >
            <Meta
              title={movie.original_title}
              description={`${movie.overview.slice(0, 20)}...`}
            />
          </Card>
        </ContentMovie>
      );
    });

    return (
      <>
        <Pagination
          onChange={handlePageChange}
          defaultCurrent={1}
          total={totalPages}
        />
        <ContainerMovies>{rederCards}</ContainerMovies>
        <Pagination
          onChange={handlePageChange}
          defaultCurrent={1}
          total={totalPages}
        />
      </>
    );
  };

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = (e) => {
    console.log(e);
    if (rating) UserateMovie(rating);
    setVisible(false);
  };

  const handleCancel = (e) => {
    console.log(e);
    setVisible(false);
  };

  let content = (
    <Spin tip="Loading...">
      <Alert message="Loading Movies" type="info" />
    </Spin>
  );
  if (!load) {
    if (!error) {
      content = renderCardsMovies();
    } else {
      content = <ErrorContent />;
    }
  }

  return (
    <Container>
      <MovieModal
        visible={visible}
        movie={movie}
        onOk={() => {
          handleOk();
        }}
        onCancel={() => {
          handleCancel();
        }}
        onChange={(value) => {
          setRating(value);
        }}
      />
      <ContainerHearder>
        <HeaderText>
          <Row>
            <ImageTitle src={movieIcon} />
            <Title>
              <b>Movies</b>
            </Title>
          </Row>
          <SubTitle>
            <b>Uma Historais de filmes</b>
          </SubTitle>
        </HeaderText>
        <AutoComplete
          options={options}
          onSearch={handleSearch}
          onChange={handleChangeSearch}
        >
          <Input.Search size="large" placeholder="Title, Movies.... " enterButton />
        </AutoComplete>
      </ContainerHearder>
      {content}
    </Container>
  );
};

export default Dashboard;
