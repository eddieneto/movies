import create from "zustand";
import {
  getMovies,
  searchMovie,
  getMoviesByPage,
  rateMovie,
} from "../../Store/api";

const moviesInitialState = {
  movies: null,
  totalPages: 0,
  rateresponse: {},
};

const [useMovies] = create((set, get) => ({
  ...moviesInitialState,
  getStateMovies: () => {
    const movies = get().movies;
  },
  setMovies: async () => {
    const response = await getMovies();
    set((state) => ({
      ...state,
      movies: response.data,
      totalPages: response.totalpages,
    }));
  },
  UseSearchMovie: async (text) => {
    const response = await searchMovie(text);
    set((state) => ({ ...state, movies: response }));
  },
  UseMoviesByPage: async (page) => {
    const response = await getMoviesByPage(page);
    set((state) => ({
      ...state,
      movies: response,
    }));
  },
  UserateMovie: async (number) => {
    const response = await rateMovie(number);
    set((state) => ({
      ...state,
      rateresponse: response,
    }));
  },
}));

export { useMovies };
