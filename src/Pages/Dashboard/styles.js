import styled, { css } from "styled-components";
import {
  Card as AntdCard,
  AutoComplete as AntdAutoComplete,
  Pagination as AntdPagination,
} from "antd";

export const Container = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  background-color: midnightblue;
`;

export const ContainerHearder = styled.div`
  padding-top: 10px;
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export const Row = styled.div`
  width: 100%;
  display: flex;
  margin-bottom: 10px;
`;

export const RowFlex = styled.div`
  width: 100%;
  display: flex;
`;

export const ContainerImage = styled.div`
  background-image: ${({ img }) => `url(${img})`};
  background-size: cover;
  background-position: center;
  height: 230px;
  border-radius: 15px 15px 0px 0px;
  width: auto;
`;

export const MovieSubtitle = styled.p``;

export const ContainerMovies = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const ContentMovie = styled.div`
  padding: 5px;
  /* border: 2px solid red; */
  border-radius: 15px 15px 0px 0px;
`;

export const Card = styled(AntdCard)`
  /* padding: 5px; */
  min-width: 240px;
  width: 240px;
  border-radius: 15px 15px 15px 15px;
  margin-right: -1px;
  margin-left: -1px;
  margin-top: -1px;
`;

export const Title = styled.h1`
  color: white;
  margin: 0;
  padding-left: 15px;
`;

export const SubTitle = styled.h4`
  color: white;
`;

export const ImageTitle = styled.img`
  height: auto;
  width: 40px;
`;

export const HeaderText = styled.div``;

export const AutoComplete = styled(AntdAutoComplete)`
  padding: 40px;
  width: 300px;
`;
export const Pagination = styled(AntdPagination)`
  padding: 10px;
  display: flex;
  justify-content: flex-end;
`;
